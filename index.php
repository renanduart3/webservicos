<?php 

// inclui o autoloader do Composer 
require 'vendor/autoload.php'; 

// inclui o arquivo de inicialização 
require 'init.php'; 

// instancia o Slim, habilitando os erros (útil para debug, em desenvolvimento) 
$app = new \Slim\App([ 'settings' => [
        'displayErrorDetails' => true
    ]
]);
  
// carrega página inicial
$app->get('/', function ()
{
    $UsersController = new \App\Controllers\UsersController;
    $UsersController->index();
});

//retorna consulta de prestador
$app->get('/lista/{consulta}', function($request)
{
    $consulta = $request->getQueryParams();
    $ListaPrestadores = new \App\Controllers\PrestadoresController;
    $ListaPrestadores->lista($consulta['consulta']);
});

//tela login
$app->get('/login', function()
{
    $LoginController = new \App\Controllers\LoginController;
    $LoginController->index();
});
//Valida Login
$app->get('/login/login', function($request)
{
    $usuario = $request->getQueryParams()['usuario'];
    $senha = $request->getQueryParams()['senha'];
    
    $LoginController = new \App\Controllers\LoginController;
    $LoginController->login($usuario,$senha);    
});
//Destroy Login
$app->get('/logoff',function()
{
    $LoginController = new \App\Controllers\LoginController;
    $LoginController->logoff();
});

// adição de usuário
// exibe o formulário de cadastro
$app->get('/add', function ()
{
    $UsersController = new \App\Controllers\UsersController;
    $UsersController->create();
});
 
// processa o formulário de cadastro
$app->post('/add', function ()
{
    $UsersController = new \App\Controllers\UsersController;
    $UsersController->store();
}); 
 
// edição de usuário
// exibe o formulário de edição
$app->get('/edit/{id}', function ($request)
{
    // pega o ID da URL
    $id = $request->getAttribute('id');
 
    $UsersController = new \App\Controllers\UsersController;
    $UsersController->edit($id);
});
 
// processa o formulário de edição
$app->post('/edit', function ()
{
    $UsersController = new \App\Controllers\UsersController;
    $UsersController->update();
});
 
// remove um usuário
$app->get('/remove/{id}', function ($request)
{
    // pega o ID da URL
    $id = $request->getAttribute('id');
 
    $UsersController = new \App\Controllers\UsersController;
    $UsersController->remove($id);
});

$app->get('/servico',function()
{
    $PrestadorController = new \App\Controllers\PrestadoresController;
    $PrestadorController->newServico();
});
 
$app->get('/lista/prestador/view/{id}',function($request)
{
    $id = $request->getAttribute('id');
    $PrestadorController = new \App\Controllers\PrestadoresController;
    $PrestadorController->view($id);
});
$app->run();