<?php
include('conexao.php');
$sql = "SELECT * FROM cad_estados ORDER BY est_sigla";
$res = mysql_query($sql, $conexao);
$num = mysql_num_rows($res);
for ($i = 0; $i < $num; $i++) {
  $dados = mysql_fetch_array($res);
  $arrEstados[$dados['est_id']] = $dados['est_sigla'];
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns='http://www.w3.org/1999/xhtml'>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script>
      
      $(function (){
        $( "#dialog-form" ).dialog({
          autoOpen: false,
          height: 300,
          width: 350,
          modal: true,
          buttons: {
            "Criar nova cidade": function() {
              salvar_cidade(); // assim que clicar em Criar, devo chamar a funcao para salvar o nome da cidade
              $( this ).dialog( "close" );
            },
            Cancel: function() {
              $( this ).dialog( "close" );
            }
          },
          close: function() {
            $('#cidade_box').val('');
          }
        });
      });
      
      
      function buscar_cidades(){
        var estado = $('#estado').val();
        if(estado){
          var url = 'ajax_buscar_cidades.php?estado='+estado;
          $.get(url, function(dataReturn) {
            $('#load_cidades').html(dataReturn);
          });
        }
      }
      
      function nova_cidade(){
        if($('#estado').val()==''){
          alert('Selecione o estado');
        }else{
          $( "#dialog-form" ).dialog( "open" );
        }
        
      }
      
      function salvar_cidade(){
        var cidade_box = $('#cidade_box').val();
        var estado = $('#estado').val();
        if(cidade_box!='' && estado!=''){
          var url = 'ajax_salvar_cidade.php?estado='+estado+'&cidade_box='+cidade_box;
          $.get(url, function() {
            buscar_cidades();
          });
        }
        
      }
    </script>


<script type="text/javascript">
// Run Select2 plugin on elements
function DemoSelect2(){
	$('#s2_with_tag').select2({placeholder: "Select OS"});
	$('#s2_country').select2();
	$('#s3_country').select2();
}
// Run timepicker
function DemoTimePicker(){
	$('#input_time').timepicker({setDate: new Date()});
}
$(document).ready(function() {
	// Create Wysiwig editor for textare
	TinyMCEStart('#wysiwig_simple', null);
	TinyMCEStart('#wysiwig_full', 'extreme');
	// Add slider for change test input length
	FormLayoutExampleInputLength($( ".slider-style" ));
	// Initialize datepicker
	$('#input_date').datepicker({setDate: new Date()});
	$('#input_date2').datepicker({setDate: new Date()});
	
	// Load Timepicker plugin
	LoadTimePickerScript(DemoTimePicker);
	// Add tooltip to form-controls
	$('.form-control').tooltip();
	LoadSelect2Script(DemoSelect2);
	// Load example of form validation
	LoadBootstrapValidatorScript(DemoFormValidator);
	// Add drag-n-drop feature to boxes
	WinMove();
});
</script>


<script type="text/javascript">
$(document).ready(function() {
	// Load TimePicker plugin and callback all time and date pickers
	LoadTimePickerScript(AllTimePickers);
	// Create jQuery-UI tabs
	$("#tabs").tabs();
	// Sortable for elements
	$(".sort").sortable({
		items: "div.col-sm-2",
		appendTo: 'div.box-content'
	});
	// Droppable for example of trash
	$(".drop div.col-sm-2").draggable({containment: '.dropbox' });
	$('#trash').droppable({
		drop: function(event, ui) {
			$(ui.draggable).remove();
		}
	});
	var icons = {
		header: "ui-icon-circle-arrow-e",
		activeHeader: "ui-icon-circle-arrow-s"
	};
	// Make accordion feature of jQuery-UI
	$("#accordion").accordion({icons: icons });
	// Create UI spinner
	$("#ui-spinner").spinner();
	// Add Drag-n-Drop to boxes
	WinMove();
});
</script>
  </head>
  <body style="font-size: 12px; font-family: 'Arial'">
    <h2>Exemplo para carregar cidade/estado por JQuery</h2>
    <form>
      <div>
        <label>Estado:</label>
        <select name="estado" id="estado" onchange="buscar_cidades()">
          <option value="">Selecione...</option>
          <?php
          foreach ($arrEstados as $value => $name) {
            echo "<option value='{$value}'>{$name}</option>";
          }
          ?>
        </select>
      </div>
      <div id="load_cidades">
        <label>Cidades:</label>
        <select name="cidade" id="cidade">
          <option value="">Selecione o estado</option>
        </select>
        <span><a href="javascript:nova_cidade()">Nova Cidade</a></span>
      </div>
    </form>

    <div id="dialog-form" title="Nova cidade">
      <form>
        <fieldset>
          <div>
            <label for="email">Cidade:</label>
            <input type="text" name="cidade_box" id="cidade_box" value="" class="text ui-widget-content ui-corner-all" />
          </div>
        </fieldset>
      </form>
    </div>
  </body>
</html>