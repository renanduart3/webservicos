<?
/**
 * Função para cifrar textos com a cifra de César
 *
 * @param     string $str o texto que deseja cifrar / decifrar
 * @param     integer $shift o número de deslocamentos que deseja efetuar
 * @return    string o texto cifrado / decifrado
 */

function criptografa($str, $shift){
    $char = range('a', 'z');
    $flip = array_flip($char);

    for ($i = 0; $i < strlen($str); $i++) {
        if (in_array(strtolower($str{$i}), $char)) {
            $ord = $flip[strtolower($str{$i})];

            $ord = ($ord + $shift) % 26;

            if ($ord < 0) $ord += 26;

            $str{$i} = ($str{$i} == strtolower($str{$i})) ? $char[$ord]
                                                          : strtoupper($char[$ord]);
        }
    }

    return $str;
}

function validaCPF($CpfCnpj){

    // Verifica se um número foi informado
    if(empty($CpfCnpj)) {
        return false;
    }
 
    // Elimina possivel mascara
    $CpfCnpj = ereg_replace('[^0-9]', '', $CpfCnpj);
    $CpfCnpj = str_pad($CpfCnpj, 11, '0', STR_PAD_LEFT);
     
    // Verifica se o numero de digitos informados é igual a 11 
    if (strlen($CpfCnpj) == 11) {
        if ($CpfCnpj == '00000000000' || 
        $CpfCnpj == '11111111111' || 
        $CpfCnpj == '22222222222' || 
        $CpfCnpj == '33333333333' || 
        $CpfCnpj == '44444444444' || 
        $CpfCnpj == '55555555555' || 
        $CpfCnpj == '66666666666' || 
        $CpfCnpj == '77777777777' || 
        $CpfCnpj == '88888888888' || 
        $CpfCnpj == '99999999999') {
        return false;
     // Calcula os digitos verificadores para verificar se o
     // CPF é válido
     } else {   
         
        for ($t = 9; $t < 11; $t++) {
             
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $CpfCnpj{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($CpfCnpj{$c} != $d) {
                return false;
            }
        }
 
        return true;
    }else 
	{
	  if (strlen($CpfCnpj) == 14){
	    // Deixa o CpfCnpj com apenas números
		$CpfCnpj = preg_replace( '/[^0-9]/', '', $cnpj );
		// Garante que o CNPJ é uma string
		$CpfCnpj = (string)$CpfCnpj;
		// O valor original
		$CpfCnpj_original = $CpfCnpj;
		// Captura os primeiros 12 números do CNPJ
		$primeiros_numeros_cnpj = substr( $CpfCnpj, 0, 12 );
	
		/**
		* Multiplicação do CNPJ
		*
		* @param string $cnpj Os digitos do CNPJ
		* @aram int $posicoes A posição que vai iniciar a regressão
		* @return int O
		*
		*/
			function multiplica_cnpj( $CpfCnpj, $posicao = 5 ) {
				// Variável para o cálculo
				$calculo = 0;
				// Laço para percorrer os item do cnpj
				for ( $i = 0; $i < strlen( $CpfCnpj ); $i++ ) {
				// Cálculo mais posição do CNPJ * a posição
				$calculo = $calculo + ( $CpfCnpj[$i] * $posicao );
				// Decrementa a posição a cada volta do laço
				$posicao--;
				// Se a posição for menor que 2, ela se torna 9
				if ( $posicao < 2 ) {
					$posicao = 9;
					}
				}
				// Retorna o cálculo
				return $calculo;
			}
	
			// Faz o primeiro cálculo
			$primeiro_calculo = multiplica_cnpj( $primeiros_numeros_cnpj );
	
			// Se o resto da divisão entre o primeiro cálculo e 11 for menor que 2, o primeiro
			// Dígito é zero (0), caso contrário é 11 - o resto da divisão entre o cálculo e 11
			$primeiro_digito = ( $primeiro_calculo % 11 ) < 2 ? 0 :  11 - ( $primeiro_calculo % 11 );
			// Concatena o primeiro dígito nos 12 primeiros números do CNPJ
			// Agora temos 13 números aqui
			$primeiros_numeros_cnpj .= $primeiro_digito;
			// O segundo cálculo é a mesma coisa do primeiro, porém, começa na posição 6
			$segundo_calculo = multiplica_cnpj( $primeiros_numeros_cnpj, 6 );
			$segundo_digito = ( $segundo_calculo % 11 ) < 2 ? 0 :  11 - ( $segundo_calculo % 11 );
			// Concatena o segundo dígito ao CNPJ
			$CpfCnpj = $primeiros_numeros_cnpj . $segundo_digito;
			// Verifica se o CNPJ gerado é idêntico ao enviado
			if ( $CpfCnpj === $cnpj_original ) {
				return true;
				}
		}
	
	}


}
}




?>