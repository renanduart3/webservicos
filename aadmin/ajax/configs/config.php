﻿<?php
/**
 * Este arquivo contém as configurações necessárias para
 * o sistema de login funcionar corretamente.
 */

/* Define o limite de tempo da sessão em 60 minutos */
session_cache_expire(60);

// Inicia a sessão
session_start();

// Variável que verifica se o usuário está logado
if ( ! isset( $_SESSION['logado'] ) ) {
    $_SESSION['logado'] = false;
}

// Erro do login
$_SESSION['login_erro'] = false;


 
// Variáveis da conexão
$base_dados  = 'b9_18954125_webservicos_hml';
$usuario_bd  = 'b9_18954125';
$senha_bd    = 'una123';
$host_db     = 'sql305.byethost9.com';
$charset_db  = 'UTF8';
$conexao_pdo = null;

// Concatenação das variáveis para detalhes da classe 
$detalhes_pdo  = 'mysql:host=' . $host_db . ';';
$detalhes_pdo .= 'dbname='. $base_dados . ';';
$detalhes_pdo .= 'charset=' . $charset_db . ';';

// Tenta conectar
try {
    // Cria a conexão com a base de dados
    $conexao_pdo = new PDO($detalhes_pdo, $usuario_bd, $senha_bd);
} catch (PDOException $e) {
    // Se der algo errado, mostra o erro 
    print "Erro: " . $e->getMessage() . "<br/>";
   
    // Mata o script
    die();
}

/**
 * Função para cifrar textos com a cifra de César
 *
 * @param     string $str o texto que deseja cifrar / decifrar
 * @param     integer $shift o número de deslocamentos que deseja efetuar
 * @return    string o texto cifrado / decifrado
 */

function criptografa($str, $shift)
{
    $char = range('a', 'z');
    $flip = array_flip($char);

    for ($i = 0; $i < strlen($str); $i++) {
        if (in_array(strtolower($str{$i}), $char)) {
            $ord = $flip[strtolower($str{$i})];

            $ord = ($ord + $shift) % 26;

            if ($ord < 0) $ord += 26;

            $str{$i} = ($str{$i} == strtolower($str{$i})) ? $char[$ord]
                                                          : strtoupper($char[$ord]);
        }
    }

    return $str;
}


?>