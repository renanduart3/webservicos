<?php 
namespace App\Models; 
use App\DB; 

class Prestador 
{
    //Função busca de serviços
    public static function selectAll($titulo = null) 
    {
        $where = '';
        if (!empty($titulo)) 
        {
            
            $where = "WHERE titulo like :titulo"; 
        }
        $sql = sprintf("SELECT * FROM servicos %s", $where); 
        $DB = new DB; 
        $stmt = $DB->prepare($sql);
 
        if (!empty($where))
        {
            $stmt->bindParam(':titulo', $titulo, \PDO::PARAM_STR);
        }
 
        $stmt->execute();
 
        $prestadores = $stmt->fetchAll(\PDO::FETCH_ASSOC);
 
        return json_encode( $prestadores);
    } 

     public static function save($user_id, $categoria_id, $subcategoria_id, $cnpj, $descriacao)  
     {
        $DB = new DB;
        $sql = "INSERT INTO users(user_id,categoria_id,subcategoria_id,cnpj,descricao)
         VALUES (:user_id, :categoria_id, :subcategoria_id, :cnpj, :descricao)";
        $stmt = $DB->prepare($sql);

        $stmt->bindValue(':user_id', $user_id);
        $stmt->bindValue(':categoria_id', $categoria_id);
        $stmt->bindValue(':subcategoria_id', $subcategoria_id);
        $stmt->bindValue(':cnpj', $cnpj);
        $stmt->bindValue(':descricao', $descriacao);
        
        if ($stmt->execute())
        {
            return true;
        }
        else
        {
            echo "Erro ao cadastrar";
            print_r($stmt->errorInfo());
            return false;
        }
     } 
}

