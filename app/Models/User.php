<?php 
namespace App\Models; 
use App\DB; 

class User 
{ 
    // Busca usuários * * Se o ID não for passado, busca todos. Caso contrário, filtra pelo ID especificado. 
    public static function selectAll($id = null) 
    { 
        $where = ''; if (!empty($id)) 
        {
             $where = 'WHERE id = :id'; 
        }
        $sql = sprintf("SELECT id, name, email, gender, birthdate FROM users %s ORDER BY name ASC", $where); $DB = new DB; $stmt = $DB->prepare($sql);
 
        if (!empty($where))
        {
            $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
        }
 
        $stmt->execute();
 
        $users = $stmt->fetchAll(\PDO::FETCH_ASSOC);
 
        return $users;
    }
 
 
    /**
     * Salva no banco de dados um novo usuário
     */
    public static function save($tipo_prestador, $name, $user_email, $cpf, $user_phone, $endereco, $cidade, $estado, $foto, $user_name, $user_password)
        {
        $user_password_hash = $user_password;

        // validação (bem simples, só pra evitar dados vazios)
        if (empty($user_name) || empty($user_email) || empty($user_password) || empty($foto))
        {
            echo "Volte e preencha todos os campos";
            return false;
        }
          
       $foto = $_FILES["foto"];
        // Pega extensão da imagem
        preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);

        // Gera um nome único para a imagem
        //	$nome_imagem = md5(uniqid(time())) . "." . $ext[1];
        $nome_imagem = $user_name . "." . $ext[1];
        
        // Caminho de onde ficará a imagem
        $caminho_imagem = "../uploadfotos/" . $nome_imagem;	
        echo "Foto : " .$caminho_imagem;
        //move_uploaded_file($foto["tmp_name"], $caminho_imagem);	
          
        // insere no banco
        $DB = new DB;
        $sql = "INSERT INTO users(tipo_usuario_id, nome, user_name, user_password_hash, foto, cpf, user_email, user_phone, endereco, estado_id, cidade_id)
         VALUES
         (:tipo_usuario_id, :nome, :user_name, :user_password_hash, :foto, :cpf, :user_email, :user_phone, :endereco, :estado_id, :cidade_id)";
        $stmt = $DB->prepare($sql);

        $stmt->bindValue(':tipo_usuario_id', $tipo_prestador);
        $stmt->bindValue(':nome', $name);
        $stmt->bindValue(':user_name', $user_name);
        $stmt->bindValue(':user_password_hash', $user_password_hash);
        $stmt->bindValue(':foto', $caminho_imagem);
        $stmt->bindValue(':cpf', $cpf);        
        $stmt->bindValue(':user_email', $user_email);
        $stmt->bindValue(':user_phone', $user_phone);
        $stmt->bindValue(':endereco', $endereco);
        $stmt->bindValue(':estado_id', $estado);
        $stmt->bindValue(':cidade_id', $cidade);
 
        if ($stmt->execute())
        {
            return true;
        }
        else
        {
            echo "Erro ao cadastrar";
            print_r($stmt->errorInfo());
            return false;
        }
    }
 
 
 
    /**
     * Altera no banco de dados um usuário
     */
    public static function update($id, $name, $email, $gender, $birthdate)
    {
        // validação (bem simples, só pra evitar dados vazios)
        if (empty($name) || empty($email) || empty($gender) || empty($birthdate))
        {
            echo "Volte e preencha todos os campos";
            return false;
        }
          
        // a data vem no formato dd/mm/YYYY
        // então precisamos converter para YYYY-mm-dd
        $isoDate = dateConvert($birthdate);
          
        // insere no banco
        $DB = new DB;
        $sql = "UPDATE users SET name = :name, email = :email, gender = :gender, birthdate = :birthdate WHERE id = :id";
        $stmt = $DB->prepare($sql);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':gender', $gender);
        $stmt->bindParam(':birthdate', $isoDate);
        $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
 
        if ($stmt->execute())
        {
            return true;
        }
        else
        {
            echo "Erro ao cadastrar";
            print_r($stmt->errorInfo());
            return false;
        }
    }
 
 
    public static function remove($id)
    {
        // valida o ID
        if (empty($id))
        {
            echo "ID não informado";
            exit;
        }
          
        // remove do banco
        $DB = new DB;
        $sql = "DELETE FROM users WHERE id = :id";
        $stmt = $DB->prepare($sql);
        $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
          
        if ($stmt->execute())
        {
            return true;
        }
        else
        {
            echo "Erro ao remover";
            print_r($stmt->errorInfo());
            return false;
        }
    }
}