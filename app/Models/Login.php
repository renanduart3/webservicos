<?php

namespace App\Models; 
use App\DB; 

 

class Login
{  
    public static function LoginUser($login,$senha) 
    {        
        $where = "WHERE user_name = :login AND user_password_hash = :senha"; 
        
        $sql = sprintf("SELECT * FROM users %s", $where); 
        $DB = new DB; 
        $stmt = $DB->prepare($sql);
        $stmt->bindValue(':login', $login, \PDO::PARAM_STR);
        $stmt->bindValue(':senha', $senha, \PDO::PARAM_STR);

        $stmt->execute();
        $usuarioLogado = $stmt->fetchObject();

        if($usuarioLogado){
            session_start();
            $_SESSION["usuarioLogado"] = serialize ($usuarioLogado);
            return json_encode($usuarioLogado);
        }
        else{

        }
    }
    
}