<div class="container-fluid">
	<!-- Div secundaria -->
	<div id="page-login" class="row">
		<!-- Div 3 -->
		<div class="col-xs-12 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
			<!-- Div 4 -->
			<h3 class="page-header2">EDIÇÃO DE USUÁRIO</h3>
			<!-- Div 5 -->
				<div class="box">
					<div class="box-content">
						<div class="text-center">						
						</div>
					<fieldset>
					<form enctype="multipart/form-data" method="post" action="edit" name="registerform">
						<legend>Tipo de Usuário:</legend>
							<div class="form-group">
								<label class="col-sm-4 control-label">Tipo de Usuário:</label>
								<div class="col-sm-8">
									<div class="radio-inline">
										<label>
											<input type="radio" value="2" name="tipo_usuario" <?php if ($user['tipo_usuario'] == '2'): ?> checked="checked" <?php endif; ?>><!-- <p class="input" style="    input:;    margin-left: 23px;    margin-top: 2px;">--> &nbsp &nbsp &nbsp Cliente <!--</p>-->
											<i class="fa fa-circle-o"></i>
										</label>
									</div>
									<div class="radio-inline">
											<label>
											<input type="radio" value="1" name="tipo_usuario" <?php if ($user['tipo_usuario'] == '1'): ?> checked="checked" <?php endif; ?>>><!--<p class="input" style="    input:;    margin-left: 23px;    margin-top: 2px;">--> &nbsp &nbsp &nbsp Prestador<!-- </p>-->
											<i class="fa fa-circle-o"></i>
											</label>
									</div>
						
								</div>
							</div>
							<br>
						</fieldset>
						<br>
						<fieldset>
						<legend>Dados Cadastrais:</legend>					

						<div class="form-group">
							<label class="control-label">Nome:</label>
							<input type="text" class="form-control " name="name" value="<?php echo $user['name']; ?>"/>
						</div>					
					
						<div class="form-group">
							<label class="control-label"  for="user_email">E-mail:</label>
							<input id="user_email"  type="email" class="form-control"  name="user_email" value="<?php echo $user['email']; ?>"  />
						</div>
										
						<div class="form-group">
							<label class="control-label">Confirme o E-mail:</label>
							<input type="email" class="form-control" name="email"  required/>
						</div>
					
						<div class="form-group">
							<label class="control-label">CPF</label>
							<input type="text" class="form-control " name="cpf" required/>
						</div>	

						<div class="form-group">
							<label class="control-label">Telefone:</label>
							<input type="text" class="form-control" name="user_phone" required/>
						</div>					
					
						<div class="form-group">
							<label class="control-label">Endereço:</label>
							<input type="text" class="form-control" name="endereco" required/>
						</div>

						<div class="form-group">
							<label class="control-label">Cidade:</label>
							<input type="text" class="form-control" name="cidade" required/>
						</div>

						<div class="form-group">
							<label class="control-label">Estado:</label>
							<input type="text" class="form-control" name="estado" required/>
						</div>
						
						<div class="form-group">
							<label class="control-label">Foto:</label>
							<input type="file" class="form-control" name="foto" required/>
						</div>
						
						</fieldset>
					
					
						<fieldset>
						
						<legend>Dados de Acesso:</legend>				
							<div class="form-group">
								<label class="control-label" for="user_name">Login:</label>
								<input id="user_name" type="text" class="form-control " pattern="[a-zA-Z0-9]{2,64}" name="user_name" required  />
							</div>	
							<div class="form-group">
								<label class="control-label" for="user_password_new">Senha:</label>
								<input type="password" class="form-control" id="user_password_new" name="user_password_new" pattern=".{6,}" required autocomplete="off"/>
							</div>
							<div class="form-group">
								<label class="control-label" for="user_password_repeat" >Confirme Senha:</label>
								<input id="user_password_repeat" type="password" name="user_password_repeat" pattern=".{6,}" required autocomplete="off" class="form-control" name="password" />
							</div>
							<div class="text-center">
								<!--<form id="example4" > -->
									<!--<a href="../index.html" class="btn btn-primary" id="btncheck" onclick="checkBTN">Cadastrar</a>						  -->
									<p><input id="field_terms" type="checkbox" required name="terms"> Li e Aceito <a href="remote.php" data-toggle="modal" data-target="#myMo"> os termos de utilização do site.</a> </p>
									<!--<a href="#myModal" data-toggle="modal" data-target="#myModal">Launch Demo Modal</a> -->
									<!-- <p><input type="submit" class="btn btn-primary" label="Cadastrar"></p> -->
									<p><input type="submit" name="register" class="btn btn-primary" value="Registrar" /><p>
								<!-- </form>	-->				 
							</div>
						</fieldset>
						</form>
					</div>
				</div>
			<!-- /Div 5 -->
		</div>
		<!-- /Div 3 -->
	</div>
	<!-- /Div secundaria -->
</div>