<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
 
        <title>Web Serviços</title>
		
		<link href="./app/Template/css/bootstrap.min.css" rel="stylesheet">
		<link href="../app/Template/css/bootstrap.min.css" rel="stylesheet">
		<link href="../app/Template/css/bootstrap.min.css" rel="stylesheet">
		
		<link href="../../../app/Template/css/bootstrap.min.css" rel="stylesheet">
		<link href="../../../app/Template/css/estilo.css" rel="stylesheet">
		<link href="./app/Template/css/estilo.css" rel="stylesheet">
		<link href="../app/Template/css/estilo.css" rel="stylesheet">
		
		

		<link href='https://fonts.googleapis.com/css?family=Quattrocento:400,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Patua+One' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		
    </head>
 
    <body>

		<header>
			<nav class="navbar navbar-default" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-navbar" aria-expanded="false">        
						<span class="sr-only">Toggle navigation</span>        
						<span class="icon-bar"></span>        
						<span class="icon-bar"></span>        
						<span class="icon-bar"></span>      
					</button>
					<a class="navbar-brand" >Menu de navegação</a>
					</div>
					<div id="collapse-navbar" class="collapse navbar-collapse">
						<ul class="nav navbar-nav navbar-left">
							<li><a title="Home" href="/">Home</a></li>
							<li><a title="Serviços" href="/lista/consulta?consulta=">Serviços</a></li>
							<li><a title="Cadastrar" href="/add">Cadastrar</a></li>
							<li><a title="Meu Painel" href="/login">Meu Painel</a></li>
						</ul>
					</div>
				</div>
			</nav>
    	</header>

    <!-- Page Content -->


        <?php if (isset($viewName)) { $path = viewsPath() . $viewName . '.php'; if (file_exists($path)) { require_once $path; } } ?>
 
		<footer>
				<div class="row">
					<div class="col-lg-12">
						<p class="text-center">Copyright &copy; WebServiços 2016</p>
					</div>
				</div>
				<!-- /.row -->
		</footer>
		<script src="./app/Template/js/jquery-3.1.1.min.js"></script>
		<script src="../../../app/Template/js/jquery-3.1.1.min.js"></script>
		<script src="../app/Template/js/jquery-3.1.1.min.js"></script>
		<script src="./app/Template/js/bootstrap.min.js"></script>
		<script src="../../../app/Template/js/bootstrap.min.js"></script>
		<script src="../app/Template/js/bootstrap.min.js"></script>
		<script src="./app/Template/js/ajuste-menu.js"></script>
		<script src="../app/Template/js/ajuste-menu.js"></script>
		<script src="../../../app/Template/js/ajuste-menu.js"></script>
		
    </body>
</html>