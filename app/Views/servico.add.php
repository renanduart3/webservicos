<div class="row">

	<div id="breadcrumb" class="col-xs-12">

		<a href="#" class="show-sidebar">

			<i class="fa fa-bars"></i>

		</a>

		<ol class="breadcrumb pull-left">

			<li><a href="index.html">Home</a></li>

			<li><a href="#">Cadastrar Serviço</a></li>

			

		</ol>

		<div id="social" class="pull-right">

			<a href="#"><i class="fa fa-google-plus"></i></a>

			<a href="#"><i class="fa fa-facebook"></i></a>

			<a href="#"><i class="fa fa-twitter"></i></a>

			<a href="#"><i class="fa fa-linkedin"></i></a>

			<a href="#"><i class="fa fa-youtube"></i></a>

		</div>

	</div>

</div>

<div class="row">

	<div class="col-xs-12 col-sm-12">

		<div class="box">

			<div class="box-header">

				<div class="box-name">

					<i class="fa fa-search"></i>

					<span>Incluir serviços</span>

				</div>

				<div class="box-icons">

					<a class="collapse-link">

						<i class="fa fa-chevron-up"></i>

					</a>

					<a class="expand-link">

						<i class="fa fa-expand"></i>

					</a>

					<a class="close-link">

						<i class="fa fa-times"></i>

					</a>

				</div>

				<div class="no-move"></div>

			</div>

			<div class="box-content">

				<h4 class="page-header">Código:</h4>

				

				

				<form enctype="multipart/form-data" method="post" action="/admin/ajax/registerservices.php" class="form-horizontal" role="form" >

					

					

						<br>

				

						<div class="form-group">

							<label class="col-sm-1 control-label">Categoria:</label>

							<div class="col-sm-4">

							<select class="populate placeholder" name="country" id="s2_country">

							<option value="" selected="selected">-- Escolha a Categoria --</option>

							<?php

							session_start();

							include("configs/config.php");

							$pdo_verifica = $conexao_pdo->prepare('select * from categoria');

							$pdo_verifica->execute();

							

							while ($rows2 =$pdo_verifica ->fetch())

							{

								//echo "<option value='".$rows['nome']."'>".$rows['idestados']."</option>";

								//echo "<option >".$rows2['NomeCategoria']."</option>";

								echo "<option value='". $rows2['idCategoria']."'>".$rows2['NomeCategoria']."</option>";

							}

							echo "</select>";

							echo "</div>";

							echo "<label class='col-sm-2 control-label'>Sub-Categoria:</label>";

							echo "<div class='col-sm-4'>";

							echo "<select class='populate placeholder' name='country2' id='s3_country'>";

							echo "<option value='' selected='selected'>-- Escolha a Sub-Categoria --</option>";

							

							$pdo_verifica2 = $conexao_pdo->prepare('select * from subcategorias');

										$pdo_verifica2->execute();

										//$results = ;

										while ($rows =$pdo_verifica2 ->fetch())

										{

											//echo "<option value='".$rows['nome']."'>".$rows['idestados']."</option>";

											//echo "<option >".$rows['nome']."</option>";

											echo "<option value='".$rows['idcategoria']."'>".$rows['nome']."</option>";

										}

							

							?>

							<!--</select>

							</div> 

							

							

							<label class="col-sm-2 control-label">Sub-Categoria:</label>

							<div class="col-sm-4">

								<select class="populate placeholder" name="country2" id="s3_country">

									<option value="" selected="selected">-- Escolha a Sub-Categoria --</option>

										<php

										

										

										$pdo_verifica2 = $conexao_pdo->prepare('select * from subcategorias where status =1');

										$pdo_verifica2->execute();

										//$results = ;

										while ($rows =$pdo_verifica2 ->fetch())

										{

											//echo "<option value='".$rows['nome']."'>".$rows['idestados']."</option>";

											echo "<option >".$rows['nome']."</option>";

										}?>-->

								</select>

							</div>

							

							

						</div>

						

						

					<br>

			

					

					

					<div class="form-group">

						<label class="col-sm-1 control-label">Título: </label>

						<div class="col-sm-9">

						<input type="text" class="form-control" placeholder="Ex: Vaga para jardineiro - Pago R$ 100,00" data-toggle="tooltip" data-placement="bottom" title="Titulo" name="nomeservico" id="nomeservico">

						</div>

					</div>

							

				

					

					<div class="form-group has-error has-feedback">

						<label class="col-sm-1 control-label">Período:</label>

						

						<div class="col-sm-2">

							<input type="text" class="form-control" placeholder="Início" name="dtinicio" id="dtinicio">

							<span class="fa fa-calendar txt-danger form-control-feedback"></span>

						</div>

						

						<div class="col-sm-2">

							<input type="text" class="form-control" placeholder="Fim" name="dtfim" id="dtfim">

							<span class="fa fa-calendar txt-danger form-control-feedback"></span>

						</div>

						

					

					</div>



					

					<div class="form-group">

						<label class="col-sm-1 control-label" for="form-styles">Descrição: </label>

						<div class="col-sm-10">

								<textarea class="form-control" rows="5" id="wysiwig_simple" name="wysiwig_simple"></textarea>

						</div>

					</div>

					

					<div class="clearfix"></div>

				

						<div class="col-sm-5">

							<div class="checkbox">

								<label>

									<input type="checkbox" checked> Concordo com os termos de uso do site para publicação do serviço. 

									<i class="fa fa-square-o small"></i>

								</label>

							</div>

						</div>

						

						

					<br>

					<br>

					

					<div class="form-group">				

						<label class="col-sm-3 control-label">Valor Oferecido: </label>		

						<div class="col-sm-5">		

							<input type="text" id="valor" name="valor" class="form-control" placeholder="Ex: R$ 100,00" data-toggle="tooltip" data-placement="bottom" title="Tooltip for name">		

						</div>		

					</div>

								

					<div class="form-group">

						<div class="col-sm-5">

							<button type="submit" class="btn btn-primary btn-label-left">

							<span><i class="fa fa-clock-o"></i></span>

								Gravar Serviço

							</button>

						</div>

					</div>

				</form>		

		    </div>

			</form>

			</div>

		</div>

	</div>

</div>





<script type="text/javascript">

// Run Select2 plugin on elements

function DemoSelect2(){

	$('#s2_with_tag').select2({placeholder: "Select OS"});

	$('#s2_country').select2();

	$('#s3_country').select2();

}

// Run timepicker

function DemoTimePicker(){

	$('#input_time').timepicker({setDate: new Date()});

}

$(document).ready(function() {

	    $("#dtinicio").datepicker({ dateFormat: "yy-mm-dd" });

    $("#dtfim").datepicker({ dateFormat: "yy-mm-dd" }).bind("change",function(){

        var minValue = $(this).val();

        minValue = $.datepicker.parseDate("yy-mm-dd", minValue);

        minValue.setDate(minValue.getDate()+1);

        $("#to").datepicker( "option", "minDate", minValue );

    })

	

});

$(document).ready(function() {

	// Create Wysiwig editor for textare

	TinyMCEStart('#wysiwig_simple', null);

	TinyMCEStart('#wysiwig_full', 'extreme');

	// Add slider for change test input length

	FormLayoutExampleInputLength($( ".slider-style" ));

	// Initialize datepicker

	$('#dtinicio').datepicker({setDate: new Date()});

	$('#dtfim').datepicker({setDate: new Date()});

	

	

	

	// Load Timepicker plugin

	LoadTimePickerScript(DemoTimePicker);

	// Add tooltip to form-controls

	$('.form-control').tooltip();

	LoadSelect2Script(DemoSelect2);

	// Load example of form validation

	LoadBootstrapValidatorScript(DemoFormValidator);

	// Add drag-n-drop feature to boxes

	WinMove();

});









</script>





<script type="text/javascript">

$(document).ready(function() {

	// Load TimePicker plugin and callback all time and date pickers

	LoadTimePickerScript(AllTimePickers);

	// Create jQuery-UI tabs

	$("#tabs").tabs();

	// Sortable for elements

	$(".sort").sortable({

		items: "div.col-sm-2",

		appendTo: 'div.box-content'

	});

	// Droppable for example of trash

	$(".drop div.col-sm-2").draggable({containment: '.dropbox' });

	$('#trash').droppable({

		drop: function(event, ui) {

			$(ui.draggable).remove();

		}

	});

	var icons = {

		header: "ui-icon-circle-arrow-e",

		activeHeader: "ui-icon-circle-arrow-s"

	};

	// Make accordion feature of jQuery-UI

	$("#accordion").accordion({icons: icons });

	// Create UI spinner

	$("#ui-spinner").spinner();

	// Add Drag-n-Drop to boxes

	WinMove();

});

</script>

