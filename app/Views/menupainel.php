<?php?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>WEB SERVIÇOS - PAINEL DO USUÁRIO</title>
		<meta name="description" content="description">
		<meta name="author" content="Projeto Una">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="/admin/plugins/bootstrap/bootstrap.css" rel="stylesheet">
		<link href="/admin/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<link href='https://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
		<link href="/admin/plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
		<link href="/admin/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
		<link href="/admin/plugins/xcharts/xcharts.min.css" rel="stylesheet">
		<link href="/admin/plugins/select2/select2.css" rel="stylesheet">
		<link href="/admin/plugins/justified-gallery/justifiedGallery.css" rel="stylesheet">
		<link href="/admin/css/style_v2.css" rel="stylesheet">
		<link href="/admin/plugins/chartist/chartist.min.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]-->
				<script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
				<script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
		<!--[endif]-->
	</head>
    <body>
		
	<header class="navbar">
		<div class="container-fluid expanded-panel">
			<div class="row">
				<div id="logo" class="col-xs-12 col-sm-2">
					<a href="#">PAINEL</a>
				</div>
				<div id="top-panel" class="col-xs-12 col-sm-10">
				</div>
			</div>
		</div>
	</header>	
<div id="main" class="container-fluid">
	<div class="row">
		<div id="sidebar-left" class="col-xs-2 col-sm-2">
			<ul class="nav main-menu">
				<li>
					<a href="/admin/ajax/dashboard.html" class="ajax-link"><!-- -->
						<i class="fa fa-dashboard"></i>
						<span class="hidden-xs">Home </span>
					</a>
				</li>
				<li>
					<a href="../servico" class="ajax-link">
						<i class="fa fa-dashboard"></i>
						<span class="hidden-xs">Criar Serviço</span>
					</a>
				</li>			
			 <li>
					<a class="ajax-link" href="../admin/ajax/calendar.html">
						 <i class="fa fa-calendar"></i>
						 <span class="hidden-xs">Minha Agenda</span>
					</a>
				 </li>
				<li>
					<a href="../admin/ajax/page_messages.html" class="ajax-link">
						<i class="fa fa-dashboard"></i>
						<span class="hidden-xs">Minhas Mensagens</span>
					</a>
				</li>
					<li>
					<a href="logoff">
						<i class="fa fa-dashboard"></i><span class="hidden-xs">Sair</span>
					</a>
				</li>
			</ul>
		</div>
		<!--Start Content-->
		<div id="content" class="col-xs-12 col-sm-10">
			
			<div id="ajax-content"></div>
		</div>
		<!--End Content-->
	</div>
</div>
<?php if (isset($viewName)) { $path = viewsPath() . $viewName . '.php'; if (file_exists($path)) { require_once $path; } } ?>
</body>