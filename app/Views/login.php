<style type="text/css">
footer{
    width: 100%;
    position:absolute;
    bottom:0;
}
.row{
	margin-right:0px;
	margin-left:0px;
}
</style>
<form method="get" action="login/login" name="loginform">
    

	<div class="container-fluid">

	<div id="page-login" class="row">

		<div class="col-xs-12 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">

		<div class="box2">

				<div class="box-content">

					<div class="text-center">

						<h2 class="page-header">WEB SERVIÇOS <br> <small>PAINEL DO USUÁRIO</small></h2>

					</div>

					<div class="form-group">

						<label class="control-label"for="usuario">Usuário:</label>

						<input class="form-control" id="user_nausuariome" type="text" name="usuario" required placeholder="Usuario" />

					</div>

					<div class="form-group">

						<label class="control-label" for="senha">Senha:</label>

						<input class="form-control" id="user_password" type="password" name="senha" autocomplete="off" required placeholder="Senha" />

					</div>

					<div class="text-center"> 
						<div class="form-group">
							<input type="submit" name="login" value="Entrar" class="btn btn-primary form-control"/>
						</div>
						<div class="form-group">
							<a href="./add" class="btn btn-primary form-control">Criar conta</a>
						</div>
					</div>

				</div>

			</div>

		</div>

	</div>

</div>

</form>

