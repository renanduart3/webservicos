    <style type="text/css">
    
    footer{
        width:100%;
        position:absolute;
        bottom:0;
    }
    .row{
        margin-left:0px;
        margin-right:0px;
    }
    .container .row{
        padding-top:10px;
        padding-bottom:10px;
    }
    </style>
    <div class="container">
        <div class="row fundo-conteudo">
            <div class="col-md-3 col-xs-3">
                <div class="conteudo-a">
                    <h4>Detalhes de:</h4>
                    <p>
                        Nome do ofertante
                    </p>
                    <a href="#"><img class="img-responsive" src="../../../app/Template/img/avatar.png" alt=""></a>
                </div>
            </div>
            <div class="col-md-9 col-xs-9">
                <div class="conteudo-b">
                    <h4>Descrição:</h4>
                    <p>
                        Descrição do trabalho desenvolvido pelo ofertante do serviço
                    </p>
                </div>
            </div>


            <div class="col-md-3 col-xs-3">
                <h4>Contato detalhes:</h4>
                <a href="#"><img src="../../../app/Template/img/FACEBOOK.png" alt="" width="40" height="30"></a><br>
                <a href="#"><img src="../../../app/Template/img/WHATS.png" alt="" width="40" height="30"></a><br>
                <a href="#"><img src="../../../app/Template/img/SKYPE.png" alt="" width="40" height="30"></a><br>
                <a href="#">@email: </a>

            </div>

            <div class="col-md-3 col-xs-6">
                <a href="#">
                    <img class="img-responsive" src="../../../app/Template/img/CHAT.png" alt="" width="100" height="100">
                </a>
            </div>
            <div class="col-md-3 col-xs-3">

                <button type="button" class="btn btn-danger pull-right">Fechar</button>

            </div>

        </div>

    </div>
