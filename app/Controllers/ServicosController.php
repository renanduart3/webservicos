<?php 
namespace App\Models; 
use App\DB; 

class Servico 
{
    public function create()
    {
        \App\View::make('servico.create');
    } 

    public function store()
    {
        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : null; 
        $titulo = isset($_POST['titulo']) ? $_POST['titulo'] : null;       
        $descricao = isset($_POST['descricao']) ? $_POST['descricao'] : null;
        $valor_hora = isset($_POST['valor_hora']) ? $_POST['valor_hora'] : null;
        $prestador_id = isset($_POST['prestador_id']) ? $_POST['prestador_id'] : null;
        $status = isset($_POST['status']) ? $_POST['status'] : null;
                
        // pega os dados do formuário
        if (User::save($user_id, $titulo, $descricao, $valor_hora, $prestador_id, $status))
        {
            \App\View::make('home');
        }
    }
}