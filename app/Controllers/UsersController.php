<?php 
namespace App\Controllers; 
use \App\Models\User; 

class UsersController { 
    // Listagem de usuários
    public function index() 
    {
        \App\View::make('index');
    }

    public function login() 
    {
        \App\View::make('login');
    }
 
    //Exibe o formulário de criação de usuário
    public function create()
    {
        \App\View::make('users.create');
    } 
 
    //Processa o formulário de criação de usuário
    public function store()
    {
        $tipo_usuario = isset($_POST['tipo_usuario']) ? $_POST['tipo_usuario'] : null; 
        $name = isset($_POST['name']) ? $_POST['name'] : null;       
        $user_email = isset($_POST['user_email']) ? $_POST['user_email'] : null;
        $cpf = isset($_POST['cpf']) ? $_POST['cpf'] : null;
        $user_phone = isset($_POST['user_phone']) ? $_POST['user_phone'] : null;
        $endereco = isset($_POST['endereco']) ? $_POST['endereco'] : null;
        $cidade = isset($_POST['cidade']) ? $_POST['cidade'] : null;
        $estado = isset($_POST['estado']) ? $_POST['estado'] : null;
        $foto = $_FILES["foto"];
        $user_name = isset($_POST['user_name']) ? $_POST['user_name'] : null;
        $user_password = isset($_POST['user_password_new']) ? $_POST['user_password_new'] : null;
        $user_password_repeat = isset($_POST['user_password_repeat']) ? $_POST['user_password_repeat'] : null;
        
        // pega os dados do formuário
        if (User::save($tipo_usuario, $name, $user_email, $cpf, $user_phone, $endereco, $cidade, $estado, $foto, $user_name, $user_password))
        {
            \App\View::make('login');
        }
    }

    //Exibe o formulário de edição de usuário
    public function edit($id)
    {
        $user = User::selectAll($id)[0];
 
        \App\View::make('users.edit',[
            'user' => $user,
        ]);
    }
  
    //Processa o formulário de edição de usuário
    public function update()
    {
        // pega os dados do formuário
        $tipo_usuario = isset($_POST['tipo_usuario']) ? $_POST['tipo_usuario'] : null; 
        $name = isset($_POST['name']) ? $_POST['name'] : null;       
        $user_email = isset($_POST['user_email']) ? $_POST['user_email'] : null;
        $cpf = isset($_POST['cpf']) ? $_POST['cpf'] : null;
        $user_phone = isset($_POST['user_phone']) ? $_POST['user_phone'] : null;
        $endereco = isset($_POST['endereco']) ? $_POST['endereco'] : null;
        $cidade = isset($_POST['cidade']) ? $_POST['cidade'] : null;
        $estado = isset($_POST['estado']) ? $_POST['estado'] : null;
        $foto = $_FILES["foto"];
        $user_name = isset($_POST['user_name']) ? $_POST['user_name'] : null;
        $user_password = isset($_POST['user_password_new']) ? $_POST['user_password_new'] : null;
        $user_password_repeat = isset($_POST['user_password_repeat']) ? $_POST['user_password_repeat'] : null;
 
        if (User::update($tipo_usuario, $name, $user_email, $cpf, $user_phone, $endereco, $cidade, $estado, $foto, $user_name, $user_password))
        {
            header('Location: /');
            exit;
        }
    } 
 
    //Remove um usuário
    public function remove($id)
    {
        if (User::remove($id))
        {
            header('Location: /');
            exit;
        }
    }
}