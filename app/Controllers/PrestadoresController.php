<?php 
namespace App\Controllers; 
use \App\Models\Prestador; 

class PrestadoresController { 

    public function lista($filtro = null)
    {
        $prestador = Prestador::selectAll($filtro);

        \App\View::make('prestador.lista', [ 'prestador' => $prestador,]);
    }

    public function view($id)
    {
        $prestador = Prestador::selectAll($id)[0];
 
        \App\View::make('prestador.view', ['prestador' => $prestador,]);
    }
    public function newServico()
    {
        \App\View::make('servico.add');
    }

    public function create()
    {
        \App\View::make('prestador.create');
    } 
    public function store()
    {
        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : null; 
        $categoria_id = isset($_POST['categoria_id']) ? $_POST['categoria_id'] : null;       
        $subcategoria_id = isset($_POST['subcategoria_id']) ? $_POST['subcategoria_id'] : null;
        $cnpj = isset($_POST['cnpj']) ? $_POST['cnpj'] : null;
        $descriacao = isset($_POST['descriacao']) ? $_POST['descriacao'] : null;
                
        // pega os dados do formuário
        if (User::save($user_id, $categoria_id, $subcategoria_id, $cnpj, $descriacao))
        {
            \App\View::make('home');
        }
    }
}
