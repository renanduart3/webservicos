<?php
namespace App\Controllers; 
use \App\Models\Login;

class LoginController { 

    public function index() 
    {
        session_start();
        if(isset($_SESSION["usuarioLogado"])){
            \App\View::make('home');
        }
        else{
            \App\View::make('login');
        }
        
    }
    public function login($usuario, $senha){
        $user = Login::LoginUser($usuario,$senha);
        if($user)
        {
            \App\View::make('home', [ 'user' => $user,]);
        }
        else{
            \App\View::make('login');
        }
    }
    public function logoff(){
        session_start();
        session_destroy();
        \App\View::make('index');
    }
}